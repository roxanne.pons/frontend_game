//---------------------------------------------------------

function hideCoin1() {
  document.getElementById('cash').play();
  gsap.to('.coin1', {duration: 0.25, opacity: 0});
  gsap.to('word1', {delay: 0.6, display: 'none'})
  gsap.to('.first-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})
}
function hideCoin2() {
  document.getElementById('cash').play();
  gsap.to('.coin2', {duration: 0.25, opacity: 0});
  gsap.to('word2', {delay: 0.6, opacity: 0})
  gsap.to('.second-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})

}
function hideCoin3() {
  document.getElementById('cash').play();
  gsap.to('.coin3', {duration: 0.25, opacity: 0});
  gsap.to('word3', {delay: 0.6, opacity: 0})
  gsap.to('.third-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})

}
function hideCoin4() {
  document.getElementById('cash').play();
  gsap.to('.coin4', {duration: 0.25, opacity: 0});
  gsap.to('word4', {delay: 0.6, opacity: 0})
  gsap.to('.fourth-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})

}
function hideCoin5() {
  document.getElementById('cash').play();
  gsap.to('.coin5', {duration: 0.25, opacity: 0});
  gsap.to('word5', {delay: 0.6, opacity: 0})
  gsap.to('.fifth-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})

}

function hideCoin6() {
  document.getElementById('cash').play();
  gsap.to('.coin6', {duration: 0.25, opacity: 0});
  gsap.to('word6', {delay: 0.6, opacity: 0})
  gsap.to('.sixth-word', {delay: 0.5, opacity: '1'});
  gsap.to('.goodjob', {delay: 0.3, duration: 0.5,  display: 'block'})
  gsap.to('.goodjob', {delay: 1, duration: 0.5, display: 'none'})

}


//---------------------------------------------------------
const container = document.querySelector('.container');
const draggableItem1 = document.getElementById('draggableItem1');
const draggableItem2 = document.getElementById('draggableItem2');
const draggableItem3 = document.getElementById('draggableItem3');
const draggableItem4 = document.getElementById('draggableItem4');
const draggableItem5 = document.getElementById('draggableItem5');


Draggable.create('#draggableItem1', {
  bounds: container,
  type: 'xy',
});

Draggable.create('#draggableItem2', {
  bounds: container,
  type: 'y',
});
Draggable.create('#draggableItem3', {
  bounds: container,
  type: 'xy',
});

Draggable.create('#draggableItem4', {
  bounds: container,
  type: 'xy',
});

Draggable.create('#draggableItem5', {
  bounds: container,
  type: 'y',
});


//---------------------------------------------------------
 
function slideMe() {
  gsap.to(".slideItem1", {duration: 2.5, y: 700, ease: "bounce", repeat: 4}); //staggeer = delay between same elements

}

function slideMe2() {
  gsap.to(".slideItem2", {duration: 1, y: -200, ease: "elastic"}); //staggeer = delay between same elements

}

function slideMe3() {
  gsap.to(".slideItem3", {duration: 2.5, x: 300, ease: "bounce", repeat: 3}); //staggeer = delay between same elements
  
}

function slideMe4() {
  gsap.to(".slideItem4", {duration: 3.4, y: 300, ease: "elastic"}); //staggeer = delay between same elements
}

function slideMe5() {
  gsap.to(".slideItem5", {duration: 1.4, x: 300, ease: "backs"}); //staggeer = delay between same elements
  gsap.to(".slideItem5", {duration: 2.4, y: -800, ease: "backs"}); //staggeer = delay between same elements
}

function slideMe6() {
  gsap.to(".slideItem6", {duration: 3.4, x: -300, ease: "bounce"}); //staggeer = delay between same elements
}

function slideMe7() {
  gsap.to(".slideItem7", {duration: 2.4, y: -150, ease: "bounce"}); //staggeer = delay between same elements
}



//---------------------------------------------------------

function hideMe() {
  gsap.to(".hideItem", {duration: 1, opacity: 0, display: 'none'});
}

function hideMe2() {
  gsap.to(".hideItem2", {duration: 1, opacity: 0, display: 'none'});
}

function hideMe3() {
  gsap.to(".hideItem3", {duration: 1, opacity: 0, display: 'none'});
}

function hideMe4() {
  gsap.to(".hideItem4", {duration: 0.3, opacity: 0, display: 'none'});
}
function hideMe5() {
  gsap.to(".hideItem5", {duration: 0.3, opacity: 0, display: 'none'});
}

//------------------------------------------------------------------------start


const button = document.querySelector('.buttonIntro');
const progress = document.querySelector('.progress');
var i = 0;
var x = 100;

progress.style.width = '0%';


button.onclick = function() {

	if (progress.style.width === '0%') {
		setInterval(speed, 450);
		
		function speed(){

		if (i < 100) {
				i++;
				progress.style.width = i + '%';
			}
		}
	}

	else{
		setInterval(sss, 20);
		function sss(){

		if (x > 0) {
			x--;
			progress.style.width = x + '%';
		}
		}
		
  }

  document.getElementById('song').play();

  gsap.to('.buttonIntro', {duration: 0.5, display: 'none'});

  gsap.to('.youDie', {duration: 0.5, display: 'block', delay: 41});
  gsap.to('.countdown3', {duration: 0.5, display: 'block', delay: 41});
  gsap.to('.countdown3', {duration: 0.5, display: 'none', delay: 42});
  gsap.to('.countdown2', {duration: 0.5, display: 'block', delay: 42});
  gsap.to('.countdown2', {duration: 0.5, display: 'none', delay: 43.5});
  gsap.to('.countdown1', {duration: 0.5, display: 'block', delay: 43.5});
  gsap.to('.gameover', {duration: 0.5, display: 'block', delay: 45.5});

}



//----------------------------------------------------------------
gsap.defaults({ease:"none"});

var windowWrap = gsap.utils.wrap(5, window.innerWidth),
windowYoyo = gsap.utils.wrapYoyo(3, window.innerWidth); //to make the ball look like it's bouncing off the edge of the screen, just subtract 100 (the width of the ball) from the window.innerWidth

gsap.to("#draggableItem3", {
y: 1000,
duration: 20,
modifiers: {
y: y => windowWrap(parseFloat(y)) + "px"
}
});

gsap.to("#draggableItem4", {
x: 3000,
duration: 10,
repeat: 5,
modifiers: {
x: x => windowYoyo(parseFloat(x)) + "px"
}
});

gsap.to("#slideItem1", {
  y: 50,
  duration: 2,
  repeat: 25,
  modifiers: {
  x: x => windowYoyo(parseFloat(x)) + "px"
  }
  });

  gsap.to("#slideItem2", {
    y: -50,
    duration: 18,
    repeat: 10,
    rotation: 180,
    modifiers: {
    x: x => windowYoyo(parseFloat(x)) + "px"
    }
    });

    gsap.to(".hidingItem3", {
      y: 500,
      duration: 10,
      repeat: 10,
      modifiers: {
      x: x => windowYoyo(parseFloat(x)) + "px"
      }
      });
      gsap.to(".hidingItem5", {
        x: 500,
        duration: 5,
        repeat: 10,
        rotation: 90,
        modifiers: {
        x: x => windowYoyo(parseFloat(x)) + "px"
        }
        });
        gsap.to(".hidingItem4", {
          x: 200,
          duration: 200,
          repeat: 50,
          rotation: 90,
          });
  
//--------------------------------------------------------------win


function validate() {
  if ( document.getElementById("code").value == 'you are all going to die' ) {
    document.getElementById('cheers').play();
    gsap.to('.youWin', {duration: 1, display: 'block'});
  }
  else {
    document.getElementById('alertSound').play();
    alert("wrong code, try again !");
  }
}

//------------------------------------- sound